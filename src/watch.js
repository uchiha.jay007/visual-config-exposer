const chokidar = require('chokidar')
const writeConfig = require('./writeConfig')

const watch = () => {
    // Generate a base config  

    // Note: Polling is used by default in the container via
    // the CHOKIDAR_USEPOLLING=1 env that is set in the container
    var watchDir = process.env['watchDir'];
    if (!watchDir) {
        watchDir = '/home/coder/project/toolkit/'
    }
    console.log(`watching dir ${watchDir}`);
    const watcher = chokidar.watch(watchDir);
  
    watcher.on('error', (error) => console.error("error"))
        .on('all', () => {
            console.log('on all')
            writeConfig(watchDir)
        })
        .on('ready', () => {
            console.log('on ready');
        })
};
  
module.exports = watch;