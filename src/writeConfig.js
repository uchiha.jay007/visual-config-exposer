const fs = require('fs')

const writeConfig = (watchDir) => {
    filePath = watchDir + "general.json";
    console.log("filePath", filePath);
    const projectConfig = returnConfig(filePath);
    console.log('project config', projectConfig);
    fs.writeFileSync(
        __dirname + '/config.json',
        JSON.stringify(projectConfig, null, 4),
    );
}

const returnConfig = (filePath) => {
    let config = {}
    const file = JSON.parse(fs.readFileSync(filePath, 'utf8'))['config'];
    Object.keys(file).forEach((key) => {
        config[key] = file[key];
    });
    return config
}

module.exports = writeConfig;
